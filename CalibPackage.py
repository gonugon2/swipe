import numpy as np
import scipy.signal as signal_power
import scipy as sp
import scipy.io as io
import matplotlib.pyplot as plt
import itertools

def GenReRAMArray(Array,Nbits):
    Array=np.round(Array).astype(int)
    Array[Array<0]-=1
    BinArray=(((Array[:,:,None] & (1<< np.arange(Nbits)))) > 0).astype(int)
    OutputArray=np.zeros((Array.shape[0],Array.shape[1],Nbits-1,2))
    OutputArray[:,:,:,0]=BinArray[:,:,0:Nbits-1]
    OutputArray[:,:,:,1]=np.expand_dims(BinArray[:,:,Nbits-1],3)
    return OutputArray

def GenBinArray(Array,Nbits):
    Array=np.round(Array).astype(int)
    Array[Array<0]-=1
    BinArray=(((Array[:,:,None] & (1<< np.arange(Nbits)))) > 0).astype(int)
    return BinArray

def GenArray(Array,Nbits,BpCell):
    N1=Array.shape[0]
    N2=Array.shape[1]
    Array=np.round(Array).astype(int)
    Array[Array<0]-=1
    BinArray=(((Array[:,:,None] & (1<< np.arange(Nbits)))) > 0).astype(int)
    OutputArray=np.zeros((Array.shape[0],Array.shape[1],Nbits-1,2))
    OutputArray[:,:,:,0]=BinArray[:,:,0:Nbits-1]
    OutputArray[:,:,:,1]=np.expand_dims(BinArray[:,:,Nbits-1],3)
    m=int((Nbits-1)/BpCell)
    ReRAMArray=np.zeros((N1,N2,m,2))
    for i in range(m):
        NextBinArray=np.zeros((N1,N2,2))
        for j in range(BpCell):
            NextBinArray= NextBinArray*2 + OutputArray[:,:,Nbits-i*BpCell-j-2,:]
        ReRAMArray[:,:,m-i-1,:]=NextBinArray
    return ReRAMArray

def getVarModel(ModelNo,BpCell):
    VarData=io.loadmat('Calibration/vardata/data{}.mat'.format(BpCell))
    return VarData['Sds'][ModelNo]/100

def QuantRead(Array, Nbits):
    return np.true_divide(np.round(Array*(np.power(2,Nbits-1)-1)),(np.power(2,Nbits-1)-1))

def ConvRead(Array, Nbits, ModelNo=5 ,Randomize=True, RandSeed=None, BpCell=2,type=1):
    Array=Array*(np.power(2,Nbits-1)-1)
    ReRAMArray=GenArray(Array,Nbits,BpCell)
    OutArray=FuncRead(ReRAMArray,type=type,Randomize=True,RandSeed=RandSeed,BpCell=BpCell,ModelNo=ModelNo)
    return np.true_divide(OutArray,(np.power(2,Nbits-1)-1))

def FuncRead(BinArray, ModelNo ,Randomize=True, RandSeed=None, BpCell=2,type=1):
    Var=1
    VarModel=getVarModel(ModelNo,BpCell)
    if RandSeed is None:
        RandSeed=np.random.randn(*BinArray.shape)
    if not Randomize:
        Var=Var*0
    if type==1:
        WriteNoise=RandSeed*Var*(2**(BpCell)-1)
        for m in range(2**BpCell):
            WriteNoise[BinArray==m]=WriteNoise[BinArray==m]*VarModel[m]
        ReRAMArray=BinArray+WriteNoise
        DiffArray=ReRAMArray[:,:,:,0]-ReRAMArray[:,:,:,1];
        Basis=np.arange(ReRAMArray.shape[2])
        Basis=np.power(2**BpCell,Basis)
        Output=np.matmul(DiffArray,Basis)
    if type==0:
        WriteNoise=RandSeed*Var*(2**(BpCell)-1)
        WriteNoise[:,:,-1]=RandSeed[:,:,-1]*Var
        ReRAMArray=BinArray+WriteNoise
        Basis=np.arange(ReRAMArray.shape[2])
        Basis=np.power(2**BpCell,Basis)
        Basis[-1]=-Basis[-1]+1
        Output=np.matmul(ReRAMArray,Basis)
    return Output


def SWIPE(InArray,Nbits,scale,RandSeed=None,BpCell=2,type=1, ModelNo=5,skew=0):
    VarMax=1
    N=InArray.shape[0]
    M=InArray.shape[1]
    ReRAMArray=np.zeros((N,M,int((Nbits-1)/BpCell),2))
    if RandSeed is None:
        RandSeed=np.random.randn(*ReRAMArray.shape)*0
    VarModel=getVarModel(4,BpCell)
    VarModelS=np.append(np.flip(VarModel),VarModel[1:(2**BpCell)])
    VarModelS=np.sqrt(np.power(VarModelS,2.0)+np.power(VarModel[0],2))
    scale=scale*(np.power(2,Nbits-1)-1)
    Array=InArray*scale
    NCells=int((Nbits-1)/BpCell)
    for k in range(NCells):
        i=k*BpCell
        ReRAMRead=FuncRead(ReRAMArray,Randomize=True,RandSeed=RandSeed,BpCell=BpCell,type=type, ModelNo=ModelNo)
        Diff=Array-ReRAMRead
        Basis=np.arange(-2**BpCell+1,2**BpCell)
        BasisScale=np.expand_dims(np.expand_dims(Basis*(np.power(2,Nbits-BpCell-1-i)),0),0)
        skewU=skew*(np.power(2,Nbits-BpCell-1-i))
        Cost=np.power((np.expand_dims(Diff,2)-BasisScale-skewU),2)+np.expand_dims(np.expand_dims(np.power(VarModelS*VarMax*np.power(2,Nbits-1-i),2),0),0)
        NextBinArrayNew=Basis[Cost.argmin(axis=2)]
        NextBinArray=NextBinArrayNew+0
        NextBinArray[NextBinArray<0]=0
        ReRAMArray[:,:,NCells-1-k,0]=np.squeeze(NextBinArray)
        NoiseTemp1=np.random.randn(*NextBinArray.shape)
        NoiseTemp=np.squeeze(NoiseTemp1)+0

        RandSeed[:,:,NCells-1-k,0]=NoiseTemp+0

        NextBinArrayN=-(NextBinArrayNew+0)
        NextBinArrayN[NextBinArrayN<0]=0
        ReRAMArray[:,:,NCells-1-k,1]=np.squeeze(NextBinArrayN)
        NoiseTemp1=np.random.randn(*NextBinArrayN.shape)
        NoiseTemp=np.squeeze(NoiseTemp1)+0
        RandSeed[:,:,NCells-1-k,1]=NoiseTemp+0
    ReRAM_Out=FuncRead(ReRAMArray,Randomize=True,RandSeed=RandSeed,BpCell=BpCell,type=type, ModelNo=ModelNo)/scale
    return ReRAM_Out

def AWGN(Data,SNR,pow=1):
    Var=np.power(10,-SNR/20)/np.sqrt(2)
    DataOut=Data+Var*np.random.randn(*Data.shape)+1j*Var*np.random.randn(*Data.shape)
    return DataOut